import React from 'react';
import axios from 'axios';

class EditItem extends React.Component {

itemsCat = [];
state = {
	categories : [],
	items : 
		  	 { "name" : "",
		  	   "price": 123,
           "category": "",
           "image" : ""
		  	 }
}

componentDidMount = () => {
	this.setState({
		items: {
			name: this.props.item.name,
			price: this.props.item.price,
			category: this.props.item.category,
			image: this.props.item.image
		}
	})
}

	editThisItem = () => {
		var myObj = {}
                  this.props.editHandler(myObj);
	}		

	render() {

		return(
		   	<div>
       			<div>
                <h2>Edit Item</h2>
                Name
            		<input value={this.state.items.name} 
                onChange={e=>{this.setState({items : {...this.state.items, name: e.target.value}})}} type="text"/><br/>
                Price
            		<input value={this.state.items.price}
                onChange={e=>{this.setState({items : {...this.state.items, price: e.target.value}})}} type="text"/><br/>

            		<select
		          	 value={this.state.items.categories}
        		  	  onChange={e =>
            			this.setState({
              			 items : {...this.state.items, category: e.target.value}}
  				           )}
	       			  >
		          	{this.state.categories.map(cat => (
            		  <option
              			key={cat._id}
              			value={cat.name}
            		  >{cat.name}
            		  </option>
          			))}
            		</select>
            		<img src={this.state.items.image} alt="item"/>
                <input
                onChange={e=>{this.setState({items : {...this.state.items, image: e.target.files[0]}})}} type="file"/><br/>
            		<button id="editTask" type="button" onClick={this.editHandler}>Submit</button>
       			</div>

       		</div>	
			)
	}
}

export default EditItem;