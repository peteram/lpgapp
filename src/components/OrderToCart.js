import React from 'react';

class OrderToCart extends React.Component {

	render() {
		return (
        <div id="parent">
            <div><img src={this.props.image}/></div>
        	<div id="child">
            	<div>{this.props.name}</div>
            	<div>Qty.:{this.props.qty}</div>
            	<div>Tot. Price:{this.props.total}</div>
            </div> 	
        </div>
		);
	} 
}

export default OrderToCart;
