import React from 'react';
import { Link } from 'react-router-dom'

class Myresto extends React.Component {

    editModeHandler = (event) => {
                  let temp = JSON.parse(JSON.stringify(this.props.comment));
                  temp.unshift(event);
//                  console.log(temp);          

                  this.props.funcComment(temp, this.props.index);
  } 

    orderButon =(name)=> {
        var  myObj = {name: "", qty: 1, price: 0, totalPrice: 123, image: ""};
         myObj.name = this.props.name;
         myObj.price = this.props.price;
         myObj.image= this.props.image;
         this.props.addOrderHandler(myObj,name);

     }
/*
	deleteButon =(name)=> {

         this.props.deleteOrderHandler(name);

     }
*/
	render() {
		return(
        <div className="parent">
        	<div className="child">
                <Link to="/admin/edit">
                    <button type='button' onClick={()=>this.props.getItemEdit(this.props.item)}>Edit</button>
                </Link>
                <button onClick={(e)=>this.props.deleteOrderHandler(this.props.id)}>Delete</button>
            	<div>{this.props.name}</div>
            	<div>{this.props.price}</div>
            	<button onClick={(e)=>this.orderButon(this.props.name)}>Order</button>
            </div> 	
            <div className="child"><img src={this.props.image}/></div>
        </div>

		);
	}
}

export default Myresto;