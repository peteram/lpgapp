import React from 'react';
import axios from 'axios';
import M from "materialize-css";

class AddItem extends React.Component {

itemsCat = [];
state = {
	categories : [],
	items : 
		  	 { "name" : "",
		  	   "price": 123,
           "category": ""
		  	 }
}

//    doInputhandler = (event) => {
//    	console.log(event.taget.value)
//            this.setState({
//              items : {
//              	name: event.target.value
//              }  
//            });
//    }


    addTaskHandler = (event) => {

          const config = {
                  headers : {
                'content-type' : 'multipart/form-data'
              }
          }
 
          let formData = new FormData();
          formData.append("name", this.state.items.name)
          formData.append("price", this.state.items.price)
          formData.append("category", this.state.items.category)
          formData.append("myImage", this.state.items.image)
/*
            let items = {name: this.state.items.name, 
                         price: this.state.items.price,
                         category: this.state.items.category,
                         image : this.state.items.image
                        };
*/
            this.props.addItemHandler(formData, config);
    }


componentDidMount() {

  M.AutoInit();
  axios.get('http://localhost:8080/categories/')
  .then(b=> {
//    console.table(b.data);
    this.setState({
      categories : b.data,
      items: {
        ...this.state.items,
        category: b.data[0].name
      }
    });
    });

//   let options = document.querySelectorAll('option');  
//    let selects = document.querySelectorAll('select');  
//    M.FormSelect.init(options, {});
  }

render () {
//  console.log(this.state.categories)

	return (
		   	<div>
       			<div>
                <h2>Add Item</h2>
              <form>  
                Name
            		<input value={this.state.items.name} 
                  onChange={e=>{this.setState({items : {...this.state.items, name: e.target.value}})}} type="text"
                />
                Price
            		<input value={this.state.items.price}
                  onChange={e=>{this.setState({items : {...this.state.items, price: e.target.value}})}} type="text"
                />
                  <select class="browser-default"
                     value={this.state.items.categories}
                     onChange={e =>
                     this.setState({
                         items : {...this.state.items, category: e.target.value}}
                     )}>
                     {this.state.categories.map(cat => (
                        <option
                           key={cat._id} value={cat.name}>{cat.name}
                        </option>
                     ))}
                  </select>
                  <input
                onChange={e=>{this.setState({items : {...this.state.items, image: e.target.files[0]}})}} type="file"/><br/>
            		<button id="addTask" type="button" onClick={this.addTaskHandler}>Submit</button>
              </form>  
       			</div>

       		</div>	

		);
}

}

export default AddItem;