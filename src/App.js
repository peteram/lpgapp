//import React from 'react';
import logo from './logo.svg';
import './App.css';
import axios from 'axios';

import React, { Component } from 'react';
//import {BrowserRouter, Route, Switch} from 'react-router-dom'
//import Navbar from './components/Navbar'
import Home from './components/Home';
import Cart from './components/Cart';
import 'materialize-css/dist/css/materialize.min.css';

import data from './data.js';
import NavBar from './components/NavBar.js'
import Myresto from './components/Myresto.js';
import OrderToCart from './components/OrderToCart.js';
import AddItem from './components/AddItem';
import EditItem from './components/EditItem';


import {BrowserRouter, Route, Link} from 'react-router-dom';

class App extends React.Component {

url  = 'http://localhost:8080/items/';
urlcat  = 'http://localhost:8080/categories/';

//  catList = ["All","Wine","Food","Drink"]

  state = {
            categories : [],
            category : "",
            myCart : [],
            items : [],
            itemEdit: {}
          }

  addHandler = (fdata, conf)=> {

//            console.log(this.state.items.image)
//           .then(function (response) {
//           console.log(response);
                        
            axios.post('http://localhost:8080/items/', fdata, conf)
            .then((res) => {
                    let tmpItems = {name: res.data.name, 
                                  price: res.data.price,
                                  category: res.data.category,
                                  image : 'http://localhost:8080/'+res.data.image
                    };
                    console.table(res.data)
                    var tempCopy = JSON.parse(JSON.stringify(this.state.items));
                        tempCopy.push(tmpItems);
                        this.setState({items : tempCopy});
            })                    
            .catch(err => console.log(err))

  }

  getItemEdit = (itemEdit) => {
    this.setState({
      itemEdit: itemEdit
    })
  }
    editHandler = (idx) => {
          var tempCopy = JSON.parse(JSON.stringify(this.state.items));
          tempCopy = tempCopy.filter((el)=> {
                        return el._id != idx
                        });
          axios.put(this.url+idx)
          .then(() => {
            this.setState({items : tempCopy})
          })                    
          .catch(err => console.log(err))
   }       

   deleteHandler = (idx) => {
          var tempCopy = JSON.parse(JSON.stringify(this.state.items));
          tempCopy = tempCopy.filter((el)=> {
                        return el._id != idx
                        });
          axios.delete(this.url+idx)
          .then(() => {
            this.setState({items : tempCopy})
          })                    
          .catch(err => console.log(err))
   }       

   addTaskHandler = (myObj,xname) => {
                   if (this.state.myCart.length > 0) {
                      var foundYou = false;
                      var cartCopy = JSON.parse(JSON.stringify(this.state.myCart));
                          cartCopy.map((elem)=>{
                          if (elem.name===xname) {
                            console.log(elem.totalPrice+elem.price)
                             foundYou = true;
                             elem.qty++;
                             elem.totalPrice = elem.totalPrice+elem.price;
                             this.setState({
                                           myCart: cartCopy
                                          });

                            }
                          });
                    } 
                    if (!foundYou) { 
                       let cartCopy = JSON.parse(JSON.stringify(this.state.myCart));
                       cartCopy.push(myObj);          
                       this.setState({
                        myCart: cartCopy
                       });
                    }                            
    }
   filterItems = (event) => {
//          console.log(event.target.id);
           if (event.target.id === "Food-only") {
              this.setState({category : "Food"});
           } else if (event.target.id === "Drink-only") {
              this.setState({category : "Drink"})
           } else if (event.target.id === "Wine-only") {
              this.setState({category : "Wine"})
            } else {
              this.setState({category : "All"})
           }
    }

componentDidMount() {
 axios.get(this.urlcat)
  .then(c=> {
              this.setState({categories : c.data});
    console.table(c.data);
    });

  axios.get(this.url)
  .then(b=> {
//    console.table(b.data);
    this.setState({items : b.data});
  })
}

  render() { 
//   console.log(this.state)
   // if (this.state.category==="") {
   //    var resto = this.state.items.map((p)=> {
   //       return <Myresto name={p.name} price={p.price} image={p.image} id={p._id}
   //                 addOrderHandler={this.addTaskHandler} deleteOrderHandler={this.deleteHandler} 
   //              />
   //  });
   // } else {
   //    var resto = this.state.items.filter((elem)=>{
   //        return elem.category === this.state.category
   //    });
   //        resto = resto.map((p)=> {
   //        return <Myresto item={p} name={p.name} price={p.price} image={p.image} id={p._id} 
   //                  addOrderHandler={this.addTaskHandler} deleteOrderHandler={this.deleteHandler}
   //                />
   //    });
   //  }

  var order = this.state.myCart.map((elem)=>{

    return <OrderToCart name={elem.name} qty={elem.qty} total={elem.totalPrice}  image={elem.image}/>;
  })

  let links =  this.state.categories.map((l, index) => (<Link key={index} id={l._id+"-only"} onClick={this.filterItems} to={"/shop/"+l.name}>{l.name}</Link>)); 
  let routes = this.state.categories.map((r) => (
                <Route path={"/shop/"+r.name} exact>
                  {
                    this.state.items.filter( i => i.category === r.name)
                    .map( p => (
                        <Myresto getItemEdit={this.getItemEdit} item={p} name={p.name} price={p.price} image={p.image} id={p._id}
                          addOrderHandler={this.addTaskHandler} deleteOrderHandler={this.deleteHandler} 
                        />
                      ))
                  }
                </Route>
    ));

  return (
    <div className="App">
      <header className="App-header">
         <BrowserRouter>      
            <NavBar/>
{/*            <nav>
              <Link to="/">Home</Link>
                {links}
            </nav>*/}
            <Route exact path="/">
              <div>
                  <AddItem addItemHandler={this.addHandler}/>
              </div>
            </Route>

            <Route path="/shop"> 
              <div>
                {links}
              </div>
              <div>  
                {routes}
              </div>  
            </Route>

            <Route path="/cart">
              <div>
                  {order}
              </div>
            </Route>
{/*
            <Route path="/admin">
              <h1>Recipe Menus</h1>
            </Route>
*/}
            <Route path="/admin/edit">
              <EditItem item={this.state.itemEdit} editItemHandler={this.editHandler}/>
            </Route>

          </BrowserRouter>
      </header>

    </div>
  );
  }
}

export default App;
