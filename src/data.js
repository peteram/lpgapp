const data = {
  "items" : [

    {
      "name": "Chivas Regal",
      "price": 123,
      "category": "Wine",
      "image": "https://image.flaticon.com/icons/svg/2451/2451795.svg"
    },
    {
      "name": "Black Label",
      "price": 123,
      "category": "Wine",
      "image": "https://image.flaticon.com/icons/svg/1415/1415974.svg"
    },
    {
      "name": "Fundador",
      "price": 123,
      "category": "Wine",
      "image": "https://image.flaticon.com/icons/svg/2451/2451734.svg"
    },
    {
      "name": "Burger",
      "price": 123,
      "category": "Food",
      "image": "https://image.flaticon.com/icons/png/512/2323/2323526.png"
    },
    {
      "name": "Pizza",
      "price": 123,
      "category": "Food",
      "image": "https://image.flaticon.com/icons/png/512/2323/2323563.png"
    },
    {
      "name": "Fries",
      "price": 123,
      "category": "Food",
      "image": "https://image.flaticon.com/icons/png/512/2323/2323544.png"
    },
    {
      "name": "Frappe",
      "price": 123,
      "category": "Drink",
      "image": "https://image.flaticon.com/icons/png/512/2323/2323561.png"
    },
    {
      "name": "Tea",
      "price": 123,
      "category": "Drink",
      "image": "https://image.flaticon.com/icons/png/512/2323/2323568.png"
    },
  ]
}

export default data;